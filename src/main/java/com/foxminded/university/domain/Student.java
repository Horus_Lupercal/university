package com.foxminded.university.domain;

import java.util.List;

import lombok.Data;

@Data
public class Student {
	
	private Integer id;
	private String firstName;
	private String lastName;
	private Integer age;
	private List<Subject> subjects;
}
