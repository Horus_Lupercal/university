package com.foxminded.university.domain;

import lombok.Data;

@Data
public class Subject {
	
	private Integer id;
	private String name;
	private String description;
}
