package com.foxminded.university.domain;

import com.foxminded.university.domain.enums.Rank;

import lombok.Data;

@Data
public class Lecturer {
	
	private Integer id;
	private String firstName;
	private String lastName;
	private Integer age;
	private Rank rank;
}
