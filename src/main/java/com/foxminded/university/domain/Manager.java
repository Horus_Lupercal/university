package com.foxminded.university.domain;

import lombok.Data;

@Data
public class Manager {
	
	private Integer id;
	private String firstName;
	private String lastName;
	private Integer age;
}
