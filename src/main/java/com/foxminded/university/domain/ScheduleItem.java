package com.foxminded.university.domain;

import lombok.Data;

@Data
public class ScheduleItem {
	
	private Integer id;
	private Integer hour;
	private Integer subjectId;
	private Integer lectorId;
	private Integer managerId;
}
