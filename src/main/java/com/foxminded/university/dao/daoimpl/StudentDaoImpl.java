package com.foxminded.university.dao.daoimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.foxminded.university.dao.StudentDao;
import com.foxminded.university.domain.Student;
import com.foxminded.university.domain.Subject;

public class StudentDaoImpl implements StudentDao<Student> {
	
	@Override
	public Optional<Student> get(int id) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}
	
	@Override
	public List<Student> getAll() {
		// TODO Auto-generated method stub
		return new ArrayList<Student>();
	}
	
	@Override
	public Optional<Student> save(Student entity) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}
	
	@Override
	public Optional<Student> update(Student update) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}
	
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Subject> showStudentSubject() {
		// TODO Auto-generated method stub
		return new ArrayList<Subject>();
	}
}
