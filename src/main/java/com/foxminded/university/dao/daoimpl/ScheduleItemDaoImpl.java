package com.foxminded.university.dao.daoimpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.foxminded.university.dao.ScheduleItemDao;
import com.foxminded.university.domain.ScheduleItem;

public class ScheduleItemDaoImpl implements ScheduleItemDao<ScheduleItem> {

	public Optional<ScheduleItem> get(int id) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

	public List<ScheduleItem> getAll() {
		// TODO Auto-generated method stub
		return new ArrayList<ScheduleItem>();
	}

	public Optional<ScheduleItem> save(ScheduleItem entity) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

	public Optional<ScheduleItem> update(ScheduleItem update) {
		// TODO Auto-generated method stub
		return Optional.empty();
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	public List<ScheduleItem> showItemsByPeriod(LocalDate from, LocalDate to) {
		// TODO Auto-generated method stub
		return new ArrayList<ScheduleItem>();
	}

	public int viewTotalLessonsHours() {
		// TODO Auto-generated method stub
		return 0;
	}

	public List<ScheduleItem> showItems() {
		// TODO Auto-generated method stub
		return new ArrayList<ScheduleItem>();
	}

	public int viewLessonsHoursByPeriod(LocalDate from, LocalDate to) {
		// TODO Auto-generated method stub
		return 0;
	}

}
