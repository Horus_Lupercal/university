package com.foxminded.university.dao;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleItemDao<T> extends Dao<T> {
	
	List<T> showItemsByPeriod(LocalDate from, LocalDate to);
	
	int viewTotalLessonsHours();
	
	List<T> showItems();
	
	int viewLessonsHoursByPeriod(LocalDate from, LocalDate to);
}
