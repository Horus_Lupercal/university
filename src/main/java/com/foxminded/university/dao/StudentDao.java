package com.foxminded.university.dao;

import java.util.List;

import com.foxminded.university.domain.Subject;

public interface StudentDao<T> extends Dao<T> {
	
	List<Subject> showStudentSubject();
}
