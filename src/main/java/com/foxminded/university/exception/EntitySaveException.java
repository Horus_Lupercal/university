package com.foxminded.university.exception;

public class EntitySaveException extends RuntimeException{
	public EntitySaveException(Class entityClass) {
		super(String.format("Entity %s can't save", entityClass));
	}
}
