package com.foxminded.university.service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.dao.daoimpl.ScheduleItemDaoImpl;
import com.foxminded.university.domain.ScheduleItem;
import com.foxminded.university.dto.ScheduleItemDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class ScheduleItemService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private ScheduleItemDaoImpl dao;
	
	public ScheduleItemService(ScheduleItemDaoImpl dao) {
		this.dao = dao;
	}
	
	public List<ScheduleItemDTO> getAllScheduleItems() {
		
		List<ScheduleItemDTO> scheduleItemsDto = dao.getAll().stream().map(source -> modelMapper.map(source, ScheduleItemDTO.class))
				.collect(Collectors.toList());

		log.traceExit(
				log.traceEntry(() -> scheduleItemsDto)
		);
		
		return scheduleItemsDto;
	}

	public ScheduleItemDTO createScheduleItem(ScheduleItemDTO create) {
		
		ScheduleItem scheduleItem = modelMapper.map(create, ScheduleItem.class);
		ScheduleItem response = dao.save(scheduleItem).orElseThrow(() -> new EntitySaveException(ScheduleItem.class));

		log.traceExit(
			    log.traceEntry(() -> create),
			    response
		);
		
		return modelMapper.map(response, ScheduleItemDTO.class);
	}

	public ScheduleItemDTO updateScheduleItem(ScheduleItemDTO update) {
		
		ScheduleItem scheduleItem = modelMapper.map(update, ScheduleItem.class);
		ScheduleItem response = dao.update(scheduleItem)
				.orElseThrow(() -> new EntityUpdateException(ScheduleItem.class, update.getId()));

		log.traceExit(
			    log.traceEntry(() -> update),
			    response
		);
		
		return modelMapper.map(response, ScheduleItemDTO.class);
	}

	public ScheduleItemDTO getScheduleItem(int id) {

		ScheduleItem response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(ScheduleItem.class, id));
		
		log.traceExit(
			    log.traceEntry(() -> id),
			    response
		);
		
		return modelMapper.map(response, ScheduleItemDTO.class);
	}

	public void deleteScheduleItem(int id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		dao.delete(id);		
	}
	
	public List<ScheduleItemDTO> findItemsByPeriod(LocalDate from, LocalDate to) {
		
		List<ScheduleItemDTO> scheduleItemsDto = dao.showItemsByPeriod(from, to).stream().map(
				item -> modelMapper.map(item, ScheduleItemDTO.class)).collect(Collectors.toList());
		
		log.traceExit(
			    log.traceEntry(() -> from.toString() +  to.toString()),
			    scheduleItemsDto
		);
		
		return scheduleItemsDto;
	}
	
	public int viewTotalLessonsHours() {
		
		int hours = dao.viewTotalLessonsHours();
		
		log.traceExit(
			    log.traceEntry(() -> hours)
		);
		
		return hours;
	}
	
	public List<ScheduleItemDTO> showItems() {
		
		List<ScheduleItemDTO> scheduleItemsDto = dao.showItems().stream().map(
				item -> modelMapper.map(item, ScheduleItemDTO.class)).collect(Collectors.toList());
		
		log.traceExit(
			    log.traceEntry(() -> scheduleItemsDto)
		);
		
		return scheduleItemsDto;
	}
	
	public int viewLessonsHoursByPeriod(LocalDate from, LocalDate to) {
		
		int hours = dao.viewLessonsHoursByPeriod(from, to);
		
		log.traceExit(
			    log.traceEntry(() -> hours)
		);
		
		return hours;
	}
}
