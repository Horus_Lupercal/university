package com.foxminded.university.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.dao.daoimpl.StudentDaoImpl;
import com.foxminded.university.domain.Student;
import com.foxminded.university.dto.StudentDTO;
import com.foxminded.university.dto.SubjectDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class StudentService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private StudentDaoImpl dao;

	public StudentService(StudentDaoImpl dao) {
		this.dao = dao;
	}

	public List<StudentDTO> getAllStudent() {
		List<StudentDTO> students = dao.getAll().stream().map(source -> modelMapper.map(source, StudentDTO.class))
				.collect(Collectors.toList());
		
		log.traceExit(
				log.traceEntry(() -> students)
		);

		return students;
	}

	public StudentDTO createStudent(StudentDTO create) {
		
		Student student = modelMapper.map(create, Student.class);
		Student response = dao.save(student).orElseThrow(() -> new EntitySaveException(Student.class));

		log.traceExit(
			    log.traceEntry(() -> create),
			    response
		);
		
		return modelMapper.map(response, StudentDTO.class);
	}

	public StudentDTO updateStudent(StudentDTO update) {
		
		Student student = modelMapper.map(update, Student.class);
		Student response = dao.update(student)
				.orElseThrow(() -> new EntityUpdateException(Student.class, update.getId()));

		log.traceExit(
			    log.traceEntry(() -> update),
			    response
		);
		
		return modelMapper.map(response, StudentDTO.class);
	}

	public StudentDTO getStudent(int id) {

		Student response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Student.class, id));
		
		log.traceExit(
			    log.traceEntry(() -> id),
			    response
		);
		
		return modelMapper.map(response, StudentDTO.class);
	}

	public void deleteStudent(int id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		dao.delete(id);		
	}
	
	public List<SubjectDTO> getStudentSubejcts() {
		List<SubjectDTO> subjects = dao.getAll().stream().map(source -> modelMapper.map(source, SubjectDTO.class))
				.collect(Collectors.toList());
		
		log.traceExit(
				log.traceEntry(() -> subjects)
		);

		return subjects;
	}
}
