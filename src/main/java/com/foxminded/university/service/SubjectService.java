package com.foxminded.university.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.dao.daoimpl.SubjectDaoImpl;
import com.foxminded.university.domain.Subject;
import com.foxminded.university.dto.SubjectDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class SubjectService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private SubjectDaoImpl dao;
	
	public SubjectService(SubjectDaoImpl dao) {
		this.dao = dao;
	}
	
	public List<SubjectDTO> getAllSubjects() {
		
		List<SubjectDTO> subjectsDto = dao.getAll().stream().map(source -> modelMapper.map(source, SubjectDTO.class))
				.collect(Collectors.toList());

		log.traceExit(
				log.traceEntry(() -> subjectsDto)
		);
		
		return subjectsDto;
	}

	public SubjectDTO createSubject(SubjectDTO create) {
		
		Subject subject = modelMapper.map(create, Subject.class);
		Subject response = dao.save(subject).orElseThrow(() -> new EntitySaveException(Subject.class));

		log.traceExit(
			    log.traceEntry(() -> create),
			    response
		);
		
		return modelMapper.map(response, SubjectDTO.class);
	}

	public SubjectDTO updateSubject(SubjectDTO update) {
		
		Subject subject = modelMapper.map(update, Subject.class);
		Subject response = dao.update(subject)
				.orElseThrow(() -> new EntityUpdateException(Subject.class, update.getId()));

		log.traceExit(
			    log.traceEntry(() -> update),
			    response
		);
		
		return modelMapper.map(response, SubjectDTO.class);
	}

	public SubjectDTO getSubject(int id) {

		Subject response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Subject.class, id));
		
		log.traceExit(
			    log.traceEntry(() -> id),
			    response
		);
		
		return modelMapper.map(response, SubjectDTO.class);
	}

	public void deleteSubject(int id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		dao.delete(id);		
	}
}
