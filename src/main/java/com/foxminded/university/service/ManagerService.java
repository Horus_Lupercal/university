package com.foxminded.university.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.dao.daoimpl.ManagerDaoImpl;
import com.foxminded.university.domain.Manager;
import com.foxminded.university.dto.ManagerDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class ManagerService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private ManagerDaoImpl dao;
	
	public ManagerService(ManagerDaoImpl dao) {
		this.dao = dao;
	}
	
	public List<ManagerDTO> getAllManagers() {
		
		List<ManagerDTO> managersDto = dao.getAll().stream().map(source -> modelMapper.map(source, ManagerDTO.class))
				.collect(Collectors.toList());

		log.traceExit(
				log.traceEntry(() -> managersDto) 
		);
		
		return managersDto;
	}

	public ManagerDTO createManager(ManagerDTO create) {
		
		Manager manager = modelMapper.map(create, Manager.class);
		Manager response = dao.save(manager).orElseThrow(() -> new EntitySaveException(Manager.class));

		log.traceExit(
			    log.traceEntry(() -> create),
			    response
		);
		
		return modelMapper.map(response, ManagerDTO.class);
	}

	public ManagerDTO updateManager(ManagerDTO update) {
		
		Manager manager = modelMapper.map(update, Manager.class);
		Manager response = dao.update(manager)
				.orElseThrow(() -> new EntityUpdateException(Manager.class, update.getId()));

		log.traceExit(
			    log.traceEntry(() -> update),
			    response
		);
		
		return modelMapper.map(response, ManagerDTO.class);
	}

	public ManagerDTO getManager(int id) {

		Manager response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Manager.class, id));
		
		log.traceExit(
			    log.traceEntry(() -> id),
			    response
		);
		
		return modelMapper.map(response, ManagerDTO.class);
	}

	public void deleteManager(int id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		dao.delete(id);		
	}
}
