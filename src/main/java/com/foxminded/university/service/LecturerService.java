package com.foxminded.university.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.foxminded.university.dao.daoimpl.LecturerDaoImpl;
import com.foxminded.university.domain.Lecturer;
import com.foxminded.university.dto.LecturerDTO;
import com.foxminded.university.exception.EntityNotFoundException;
import com.foxminded.university.exception.EntitySaveException;
import com.foxminded.university.exception.EntityUpdateException;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class LecturerService {
	
	private ModelMapper modelMapper = new ModelMapper();
	private LecturerDaoImpl dao;
	
	public LecturerService(LecturerDaoImpl dao) {
		this.dao = dao;
	}
	
	public List<LecturerDTO> getAllLecturers() {
		return lecturersToLecturersDTO(dao.getAll());
	}

	public LecturerDTO createLecturer(LecturerDTO create) {
		
		Lecturer lecturer = modelMapper.map(create, Lecturer.class);
		Lecturer response = dao.save(lecturer).orElseThrow(() -> new EntitySaveException(Lecturer.class));

		log.traceExit(
			    log.traceEntry(() -> create),
			    response
		);
		
		return modelMapper.map(response, LecturerDTO.class);
	}

	public LecturerDTO updateLecturer(LecturerDTO update) {
		
		Lecturer lecturer = modelMapper.map(update, Lecturer.class);
		Lecturer response = dao.update(lecturer)
				.orElseThrow(() -> new EntityUpdateException(Lecturer.class, update.getId()));

		log.traceExit(
			    log.traceEntry(() -> update),
			    response
		);
		
		return modelMapper.map(response, LecturerDTO.class);
	}

	public LecturerDTO getLecturer(int id) {

		Lecturer response = dao.get(id).orElseThrow(() -> new EntityNotFoundException(Lecturer.class, id));
		
		log.traceExit(
			    log.traceEntry(() -> id),
			    response
		);
		
		return modelMapper.map(response, LecturerDTO.class);
	}

	public void deleteLecturer(int id) {
		
		log.traceExit(log.traceEntry(() -> id));
		
		dao.delete(id);		
	}
	
	private List<LecturerDTO> lecturersToLecturersDTO(List<Lecturer> lecturers) {

		List<LecturerDTO> lecturersDto = lecturers.stream().map(source -> modelMapper.map(source, LecturerDTO.class))
				.collect(Collectors.toList());

		log.traceExit(
				log.traceEntry(() -> lecturers), 
				lecturersDto
		);
		
		return lecturersDto;
	}
}
