package com.foxminded.university.dto;

import com.foxminded.university.domain.enums.Rank;

import lombok.Data;

@Data
public class LecturerDTO {
	
	private Integer id;
	private String firstName;
	private String lastName;
	private Integer age;
	private Rank rank;
}
