package com.foxminded.university.dto;

import lombok.Data;

@Data
public class SubjectDTO {
	
	private Integer id;
	private String name;
	private String description;
}
