package com.foxminded.university.dto;

import java.util.List;

import lombok.Data;

@Data
public class StudentDTO {
	
	private Integer id;
	private String firstName;
	private String lastName;
	private Integer age;
	private List<SubjectDTO> subjects;
}

