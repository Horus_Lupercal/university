package com.foxminded.university.dto;

import lombok.Data;

@Data
public class ScheduleItemDTO {
	
	private Integer id;
	private Integer hour;
	private Integer subjectId;
	private Integer lectorId;
	private Integer managerId;
}
